﻿using System.Collections.Generic;
using System.Linq;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence;

namespace SonicHRM.DomainLogics
{
    public class DesignationLogics
    {
        private readonly DesignationDataAccess _designationDataAccess;

        public DesignationLogics(DesignationDataAccess designationDataAccess)
        {
            _designationDataAccess = designationDataAccess;
        }

        public List<Designation> GetAllDesignations()
        {
            var result = _designationDataAccess.GetAll().ToList();
            return result;
        }

        public bool SaveDesignation(Designation designation)
        {
            var result = _designationDataAccess.Add(designation);
            return result;
        }
    }
}
