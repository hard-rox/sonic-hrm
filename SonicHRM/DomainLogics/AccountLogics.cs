﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using SonicHRM.Helpers;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Models.Enums;
using SonicHRM.Models.ViewModels;
using SonicHRM.Persistence;

namespace SonicHRM.DomainLogics
{
    public class AccountLogics
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly AccountDataAccess _accountDataAccess;
        private readonly PunchDataAccess _punchDataAccess;

        public AccountLogics(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, AccountDataAccess accountDataAccess, PunchDataAccess punchDataAccess)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _accountDataAccess = accountDataAccess;
            _punchDataAccess = punchDataAccess;
        }

        public IdentityResult CreateUser(ApplicationUser newUser)
        {
            newUser.CreatedAt = StaticProperties.GetCurrentDateTime();
            newUser.UpdatedAt = StaticProperties.GetCurrentDateTime();
            newUser.PermittedMenues = new List<PermittedMenu>()
            {
                new PermittedMenu()
                {
                    MenuId = 2,
                    CreatedAt = StaticProperties.GetCurrentDateTime(),
                    UpdatedAt = StaticProperties.GetCurrentDateTime()
                }
            };
            var result = _userManager.CreateAsync(newUser, "123456").Result;
            return result;
        }

        public SignInResult Login(LoginViewModel loginInfo)
        {
            var result = _signInManager
                .PasswordSignInAsync(loginInfo.Email, loginInfo.Password, loginInfo.RememberMe, false).Result;
            if (result != SignInResult.Success) return result;
            var userId = _accountDataAccess.GetSingle(au => au.Email == loginInfo.Email).Id;
            _punchDataAccess.Add(new Punch()
            {
                ApplicationUserId = userId,
                WorkingDateTime = StaticProperties.GetCurrentDateTime(),
                Punchtype = Punchtype.InPunch
            });
            return result;
        }

        public void Logout(string userId)
        {
            var result = _signInManager.SignOutAsync();
            _punchDataAccess.Add(new Punch()
            {
                ApplicationUserId = userId,
                WorkingDateTime = StaticProperties.GetCurrentDateTime(),
                Punchtype = Punchtype.OutPunch
            });
        }

        public ApplicationUser GetUser(string id)
        {
            var user = _accountDataAccess.GetSingle(au => au.Id == id);
            return user;
        }

        public ApplicationUser GetUserByEmail(string email)
        {
            var user = _accountDataAccess.GetSingle(au => au.Email == email);
            return user;
        }

        public List<ApplicationUser> GetAllEmployees()
        {
            var result = _accountDataAccess.GetAll();
            return result.ToList();
        }

    } 
}
