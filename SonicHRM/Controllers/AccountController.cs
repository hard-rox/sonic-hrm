﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SonicHRM.DomainLogics;
using SonicHRM.Models.ViewModels;

namespace SonicHRM.Controllers
{
    public class AccountController : Controller
    {
        private readonly AccountLogics _accountManager;

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error.Description);
            }
        }

        public AccountController(AccountLogics accountManager)
        {
            _accountManager = accountManager;
        }

        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login([FromForm]LoginViewModel loginInfo)
        {
            try
            {
                if (!ModelState.IsValid) return View(loginInfo);

                var result = _accountManager.Login(loginInfo);
                if (result.Succeeded)
                    return RedirectToAction("Index", "Home");

                ModelState.AddModelError("", "Invalid Login.");
                return View(loginInfo);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return RedirectToAction("Error", "Home");
            }
        }

        public IActionResult Logout()
        {
            try
            {
                var userId = this.User.FindFirstValue(ClaimTypes.NameIdentifier);
                _accountManager.Logout(userId);
                return RedirectToAction("Login");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return RedirectToAction("Error", "Home");
            }
        }

        //public IActionResult Register()
        //{
        //    return View();
        //}

        //[HttpPost]
        //public IActionResult Register([FromForm]RegisterViewModel registerInfo)
        //{
        //    try
        //    {
        //        var result = _accountManager.CreateUser(registerInfo);
        //        if (result.Succeeded)
        //            return RedirectToAction("Login", "Account");

        //        AddErrors(result);
        //        return View(registerInfo);
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        return RedirectToAction("Error", "Home");
        //    }
        //}

        public IActionResult AccessDenied(string returnUrl)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }
    }
}