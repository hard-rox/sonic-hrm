﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class MenuSeedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "204d1137-203f-4eb5-9f28-86811bc52643", "24fadc59-c7b0-47e5-b49c-83dd56378606" });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "62eefddb-ebb0-4729-a31d-366cee2658ff", "665b0e04-b3f7-4484-8100-33b27451b659", "Developer", null });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "Index", "fa fa-user", "Employee Management" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "SalarySetup", "Setup", "fa fa-money", "Salary Setup", 3 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "62eefddb-ebb0-4729-a31d-366cee2658ff", "665b0e04-b3f7-4484-8100-33b27451b659" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "204d1137-203f-4eb5-9f28-86811bc52643", "24fadc59-c7b0-47e5-b49c-83dd56378606", "Developer", null });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "NewEmployee", "fa fa-user-plus", "New Employee" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "Index", "Employee", "fa fa-users", "All Employee", 5 });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[,]
                {
                    { 9, "SalarySetup", "Setup", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-money", false, "Salary Setup", 3, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) },
                    { 8, "EditEmployee", "Employee", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-user", false, "Edit Employee", 5, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) }
                });
        }
    }
}
