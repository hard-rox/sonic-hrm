﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class SalaryRemod2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "d886008b-c591-498c-b921-9517f2856181", "a71270ca-c987-4f51-9369-bc351d05de3f" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "a46027b0-fc99-4917-be98-afc3197c96c3", "893f7720-e31b-4888-b1bb-8e0e1f8a5d33", "Developer", null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "a46027b0-fc99-4917-be98-afc3197c96c3", "893f7720-e31b-4888-b1bb-8e0e1f8a5d33" });

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[] { "d886008b-c591-498c-b921-9517f2856181", "a71270ca-c987-4f51-9369-bc351d05de3f", "Developer", null });
        }
    }
}
