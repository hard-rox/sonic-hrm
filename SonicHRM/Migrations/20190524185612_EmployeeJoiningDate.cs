﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SonicHRM.Migrations
{
    public partial class EmployeeJoiningDate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "0b63acf3-ca18-40bf-ad43-6fc7eb6fc229", "7999f7db-ac2e-4760-a43e-2de8ea751403" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "35d1d3e5-dc76-4fe9-a70e-481114dc654e", "2552088e-635f-4447-bc33-8c21f24d1dd9" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "3f9459c8-cd0b-45a5-bbba-403453bc161a", "1c9d33b5-3bbf-4def-96f7-3dfd578f8261" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "c3a6eb5f-14e9-4b50-86cb-8a1d66e65941", "877e5c90-887e-4909-87bf-e3bc99912a5a" });

            migrationBuilder.AddColumn<DateTime>(
                name: "JoiningDate",
                table: "Employees",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "53c39ea1-4881-44b0-9bad-23c32c377522", "8b7ca4b3-59cb-48f4-8d11-b92d80b6423e", "Developer", null },
                    { "3c27230e-78b1-496e-8b63-0d036717c5d8", "dcea6c36-e337-4b57-ac25-f5f572128299", "Admin", null },
                    { "55deb667-60fa-4e2b-ba02-3d982c47cdba", "553b266d-3b89-4280-bf80-a877b65ed82b", "HR Manager", null },
                    { "6f766646-986c-4635-9c8a-82e033fa3a82", "7b923f79-06ec-49a1-8352-0f04ec2e1df0", "Employee", null }
                });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 1,
                column: "ActionName",
                value: "Dashboard");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName" },
                values: new object[] { "Index", "Home", "fa fa-user", "Profile" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "", "", "fa fa-cogs", "Setup", null });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "DesignationSetup", "Setup", "fa fa-black-tie", "Designation Setup", 2 });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "", "", "fa fa-users", "Employee", null });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "NewEmployee", "fa fa-user-plus", "New Employee" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "Index", "fa fa-users", "All Employee" });

            migrationBuilder.InsertData(
                table: "Menus",
                columns: new[] { "Id", "ActionName", "ControllerName", "CreatedAt", "DeletedAt", "IconClass", "IsDeleted", "MenueName", "ParentMenuId", "UpdatedAt" },
                values: new object[] { 8, "EditEmployee", "Employee", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "fa fa-user", false, "Edit Employee", 4, new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified) });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "3c27230e-78b1-496e-8b63-0d036717c5d8", "dcea6c36-e337-4b57-ac25-f5f572128299" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "53c39ea1-4881-44b0-9bad-23c32c377522", "8b7ca4b3-59cb-48f4-8d11-b92d80b6423e" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "55deb667-60fa-4e2b-ba02-3d982c47cdba", "553b266d-3b89-4280-bf80-a877b65ed82b" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { "6f766646-986c-4635-9c8a-82e033fa3a82", "7b923f79-06ec-49a1-8352-0f04ec2e1df0" });

            migrationBuilder.DeleteData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DropColumn(
                name: "JoiningDate",
                table: "Employees");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "35d1d3e5-dc76-4fe9-a70e-481114dc654e", "2552088e-635f-4447-bc33-8c21f24d1dd9", "Developer", null },
                    { "0b63acf3-ca18-40bf-ad43-6fc7eb6fc229", "7999f7db-ac2e-4760-a43e-2de8ea751403", "Admin", null },
                    { "3f9459c8-cd0b-45a5-bbba-403453bc161a", "1c9d33b5-3bbf-4def-96f7-3dfd578f8261", "HR Manager", null },
                    { "c3a6eb5f-14e9-4b50-86cb-8a1d66e65941", "877e5c90-887e-4909-87bf-e3bc99912a5a", "Employee", null }
                });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 1,
                column: "ActionName",
                value: "Index");

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName" },
                values: new object[] { "", "", "fa fa-cogs", "Setup" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "DesignationSetup", "Setup", "fa fa-black-tie", "Designation Setup", 2 });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "", "", "fa fa-users", "Employee", null });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "ActionName", "ControllerName", "IconClass", "MenueName", "ParentMenuId" },
                values: new object[] { "NewEmployee", "Employee", "fa fa-user-plus", "New Employee", 4 });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 6,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "Index", "fa fa-users", "All Employee" });

            migrationBuilder.UpdateData(
                table: "Menus",
                keyColumn: "Id",
                keyValue: 7,
                columns: new[] { "ActionName", "IconClass", "MenueName" },
                values: new object[] { "EditEmployee", "fa fa-user", "Edit Employee" });
        }
    }
}
