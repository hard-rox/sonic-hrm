﻿using System;

namespace SonicHRM.Models.ApplicationModels
{
    public class Image
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string ImageDataBase64 { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
