﻿using System;
using System.Collections.Generic;

namespace SonicHRM.Models.ApplicationModels
{
    public class Menu
    {
        public int Id { get; set; }
        public string MenueName { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public string IconClass { get; set; }

        public int? ParentMenuId { get; set; }
        public virtual Menu ParentMenu { get; set; }

        public virtual ICollection<Menu> ChieldMenues { get; set; }
        public virtual ICollection<PermittedMenu> MenuPermissions { get; set; }

        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletedAt { get; set; }
    }
}
