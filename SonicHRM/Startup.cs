﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using SonicHRM.DomainLogics;
using SonicHRM.Helpers;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1)
                .AddJsonOptions( o => o.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore);

            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies()
                .UseSqlServer(Configuration.GetConnectionString("LocalDbConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.Password.RequiredLength = 6;
                    options.Password.RequiredUniqueChars = 0;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                })
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            //DataAccess Dependencies...
            services.AddScoped(typeof(AccountDataAccess));
            services.AddScoped(typeof(DesignationDataAccess));
            services.AddScoped(typeof(SalaryDataAccess));
            services.AddScoped(typeof(PunchDataAccess));
            services.AddScoped(typeof(MenuDataAccess));

            //DomainLogics Dependencies...
            services.AddScoped(typeof(AccountLogics));
            services.AddScoped(typeof(DesignationLogics));
            services.AddScoped(typeof(SalaryLogics));
            services.AddScoped(typeof(PunchLogices));
            services.AddScoped(typeof(MenuLogics));

            //Custom Authorization Requirment
            //Goes after DI coz it user dependencies...
            services.AddAuthorization(options =>
            {
                options.AddPolicy("CustomAuthorization", policy =>
                {
                    policy.Requirements.Add(
                        ActivatorUtilities.CreateInstance<CustomAuthorizationRequirment>(
                            services.BuildServiceProvider()));
                });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseAuthentication();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
