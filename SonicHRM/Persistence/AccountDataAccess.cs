﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class AccountDataAccess : GenericDataAccess<ApplicationUser>
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public AccountDataAccess(ApplicationDbContext db) : base(db)
        {
            _applicationDbContext = db;
        }
    }
}
