﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using SonicHRM.Helpers;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class GenericDataAccess<T> where T : class
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly DbSet<T> _targetTable;

        // ReSharper disable once MemberCanBeMadeStatic.Local
        private void TrySetProperty(object obj, string property, object value)
        {
            var prop = obj.GetType().GetProperty(property, BindingFlags.Public | BindingFlags.Instance);
            if (prop != null && prop.CanWrite)
                prop.SetValue(obj, value, null);
        }

        public GenericDataAccess(ApplicationDbContext db)
        {
            _dbContext = db;
            _targetTable = _dbContext.Set<T>();
        }

        public virtual bool Add(T entity)
        {
            TrySetProperty(entity, "CreatedAt", StaticProperties.GetCurrentDateTime());
            TrySetProperty(entity, "UpdatedAt", StaticProperties.GetCurrentDateTime());
            _targetTable.Add(entity);
            return _dbContext.SaveChanges() > 0;
        }

        public virtual ICollection<T> GetAll()
        {
            return _targetTable
                //.Where(e => !(bool)e.GetType().GetProperty("IsDeleted").GetValue(e))
                .ToList();
        }

        public virtual T GetSingle(Expression<Func<T, bool>> conditionExpression)
        {
            return (T)_targetTable
                //.Where(e => !(bool)e.GetType().GetProperty("IsDeleted").GetValue(e))
                .FirstOrDefault(conditionExpression);
        }

        public bool Update(T entity)
        {
            TrySetProperty(entity, "UpdatedAt", StaticProperties.GetCurrentDateTime());
            _targetTable.Attach(entity);
            _dbContext.Entry(entity).State = EntityState.Modified;
            return _dbContext.SaveChanges() > 0;
        }

        public virtual bool Delete(T entity)
        {
            TrySetProperty(entity, "IsDeleted", true);
            TrySetProperty(entity, "DeletedAt", StaticProperties.GetCurrentDateTime());
            _dbContext.Entry(entity).State = EntityState.Modified;
            //_targetTable.Remove(entity);
            return _dbContext.SaveChanges() > 0;
        }
    }

}
