﻿using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class MenuDataAccess : GenericDataAccess<Menu>
    {
        public MenuDataAccess(ApplicationDbContext db) : base(db)
        {
        }
    }
}
