﻿using SonicHRM.Models.ApplicationModels;
using SonicHRM.Persistence.DataContext;

namespace SonicHRM.Persistence
{
    public class PunchDataAccess : GenericDataAccess<Punch>
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public PunchDataAccess(ApplicationDbContext db) : base(db)
        {
            _applicationDbContext = db;
        }
    }
}
