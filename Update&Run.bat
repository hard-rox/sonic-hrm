@echo off
color a

git pull origin master

dotnet ef database update --project SonicHRM

dotnet build
dotnet run --project SonicHRM

PAUSE